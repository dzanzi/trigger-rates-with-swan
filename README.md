# Trigger Rates with SWAN

Jupyter notebooks to be run in SWAN to plot online trigger rates from pbeast, compare with predicted rates and check prescales.
This is meant to run outside P1 and reads pbeast data that are stored on EOS after a week [DaqHltPBeast](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltPBeast#8_Access_P_BEAST_Data). More recent data can be accessed from inside P1 or via graphana (see links at the bottom).


# Setup

- Log into SWAN: https://swan.cern.ch/
- Create a new project by dowloading from git https://gitlab.cern.ch/dzanzi/trigger-rates-with-swan.git (use the cloud button at the top right). This will download also the beauty submodule (https://gitlab.cern.ch/atlas-tdaq-software/beauty)
- Setup the tdaq release by changing configuration: clink on the top right "..." button in "My Projects" home page, then set `$CERNBOX_HOME/SWAN_projects/trigger-rates-with-swan/tdaq_setup.sh` in environment script, then clink on "Start my session" (`$CERNBOX_HOME` should point to your EOS home directory and the newly created SWAN project should have been created in the directory `SWAN_projects/`. If not, change the path accordingly.)

# Run

- Click on `rates.ipynb` and run the notebook. It will create plots that you can download from the SWAN project page.
- To plot the L1 and HLT prescales, you will need an intermadiate step when you need to run in a terminal on lxplus the script `source download_keys.sh` as explained in the notebook

# Personalize

- You can edit the `trigger_list` list and add all the triggers you want to plot (the more trigger, the more time it takes to plot). As suggestions, you can find the list of primary triggers in [LowestUnprescaled](https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled)
- You can change the time interval (the longer, the more time it takes to plot)

# External inputs

- Download locally the json file with the predicted rates from a recent reprocessing, eg in [RateProcessings-2022](https://atlas-trig-cost.web.cern.ch/?dir=RateProcessings-2022), delete the current `rates.json` file in the SWAN project and upload your new file to your project folder with the upload buttom

# Useful links

- [Graphana HLT Overview](https://atlasop.cern.ch/tdaq/pbeastDashboard/d/000000034/hlt-overview?orgId=1&refresh=30s&from=now-24h&to=now)
- [Graphana Rates](https://atlasop.cern.ch/tdaq/pbeastDashboard/d/iItbciwWk/full-trp-dashboard?orgId=1&from=now-12h&to=now)









