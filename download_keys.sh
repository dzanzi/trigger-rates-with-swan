#!/bin/sh
source tdaq_setup.sh

while read p; do
  extractPrescales.py --dbalias TRIGGERDB_RUN3 --l1psk ${p}
done < L1_keys.txt

while read p; do
  extractPrescales.py --dbalias TRIGGERDB_RUN3 --hltpsk ${p}
done < HLT_keys.txt

